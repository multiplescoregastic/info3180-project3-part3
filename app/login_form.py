from flask.ext.wtf import Form
from flask.ext.uploads import *
from wtforms import TextField, PasswordField, BooleanField, SubmitField
from wtforms.validators import InputRequired
from wtforms import validators

class Login_Form(Form):
  openid = TextField('openid', validators=[InputRequired()])
  username = TextField('username', validators=[InputRequired()])
  password = PasswordField('password', validators=[InputRequired()])
  remember_me = BooleanField('remember_me', default=False)
  login = SubmitField('Login', default=False)
