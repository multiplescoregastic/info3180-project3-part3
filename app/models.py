from app import db

followers = db.Table(
    'followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('profile.userid')),
    db.Column('followed_id', db.Integer, db.ForeignKey('profile.userid'))
)


class Profile(db.Model):
  userid = db.Column(db.Integer, primary_key=True, nullable=False, unique=True)
  fname = db.Column(db.String(80),nullable=False, index=True)
  lname = db.Column(db.String(80), nullable=False, index=True)
  username = db.Column(db.String(80), index=True, nullable=False, unique=True)
  email = db.Column(db.String(80), index=True, nullable=False, unique=True)
  password = db.Column(db.String(80), index=True, nullable=False)
  sex = db.Column(db.String(10), nullable=False, index=True)
  age = db.Column(db.String(25), nullable=False, index=True)

  profile_add_on = db.Column(db.String(80), nullable=False)
  image = db.Column(db.String(80), nullable=False)
  confirmation_id = db.Column(db.String(80), index=True, nullable=False, unique=True)
  confirmation_status = db.Column(db.String(5), index=True, nullable=False)
  
  is_logged_in = db.Column(db.String(4), index=True)
  
  posts = db.relationship('Post', backref='author', lazy='dynamic')
  about_me = db.Column(db.String(140))
  last_seen = db.Column(db.DateTime)
  followed = db.relationship('Profile',
                               secondary=followers,
                               primaryjoin=(followers.c.follower_id == userid),
                               secondaryjoin=(followers.c.followed_id == userid),
                               backref=db.backref('followers', lazy='dynamic'),
                               lazy='dynamic')

  def follow(self, profile):
    if not self.is_following(profile):
      self.followed.append(profile)
      return self

  def unfollow(self, profile):
    if self.is_following(profile):
      self.followed.remove(profile)
      return self

  def is_following(self, profile):
    return self.followed.filter(followers.c.followed_id == profile.userid).count() > 0

  def followed_post(self):
    return Post.query.join(followers, (follows.c.followed_id == Post.user_id)).filter(followers.c.follower_id == self.userid).order_by(Post.timestamp.desc())
  


  def is_authenticated(self):
    return True

  def is_active(self):
    return True

  def is_anonymous(self):
    return False

  # def is_logged_in(self):
  #   return False

  def get_id(self):
    try:
      return unicode(self.userid)
    except NameError:
      return str(self.userid)

  def __repr__(self):
    return "<User %r>" % (self.username)



class Post(db.Model):
  id = db.Column(db.Integer, primary_key=True, nullable=False, unique=True)
  comment = db.Column(db.String(140))
  timestamp = db.Column(db.DateTime)
  user_id = db.Column(db.Integer, db.ForeignKey('profile.userid'))

  def __repr__(self):
    return '<Post %r>' % (self.comment)
