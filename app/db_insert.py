from . import db, models
import time
import os
import uuid
from sendemail import *
#from find_ip import * 

def insert(fname1, lname1, username1, email1, password1, sex1, age1, image1):
  confirmation_id1 =str(uuid.uuid4());
  confirmation_status1 = "No"

  msg = "This is the CEO, I would like to say thank you for signing up with us. "
  msg += "To complete your registration, please confirm your email by clicking the links: "
  msg += "http://info3180-final-project.herokuapp.com/signup/confirm/" + str(confirmation_id1) + " "
  #msg += "or http://" + str(get_ip_address("wlan0")) + ":8081/sigup/confirm/" + str(confirmation_id) + " "
  #msg += "or http://" + str(get_ip_address("lo")) + ":8081/sigup/confirm/" + str(confirmation_id)
  sendemail(fname1, email1, "Confirmation", msg)
  entry = models.Profile(fname=fname1, lname=lname1, username=username1, email=email1, password=password1, sex=sex1, age=age1, profile_add_on=time.strftime("%a %d %b %Y"), image=image1, confirmation_id=confirmation_id1, confirmation_status=confirmation_status1)
  db.session.add(entry)
  db.session.commit()

  #make user follower of himself
  db.session.add(entry.follow(entry))
  db.session.commit()