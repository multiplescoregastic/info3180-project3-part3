from app import app, db, models, lm
from flask import render_template, request, redirect, url_for, flash, jsonify, session, send_file, redirect, g
from flask.ext.login import login_user, current_user, logout_user, login_required
import os
from os import path
from werkzeug import secure_filename
from flask_wtf.file import FileField
import json
from .forms import Profiler
from .login_form import Login_Form
from .profile_update import *
from db_insert import *
import time
import pdb

@lm.user_loader
def load_user(userid):
  return models.Profile.query.get(int(userid))

@app.before_request
def before_request():
    g.user = current_user

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return render_template('logout.html', title='Logout', current_user=current_user)

#main page
@app.route('/')
@app.route('/index')
@login_required
def index():
  return render_template('index.html', title="Home")

#allow user to login
@app.route('/login', methods=['GET', 'POST'])
def login():
  if g.user is not None and g.user.is_authenticated():
    return redirect(url_for('index'))
  
  form = Login_Form()
  # pdb.set_trace()
  if request.method == 'POST':
    user_exist = models.Profile.query.filter_by(username=form.username.data, password=form.password.data).first()
    
    if((user_exist is None)==True):
      flash("Sorry user doesn't exist")
      return redirect(url_for('login'))
  
    elif((user_exist is None)==False):
      user = load_user(user_exist.userid)
      login_user(user, remember=form.remember_me.data)
      flash("Successful Login")
      return redirect(url_for('index'))
    
  return render_template('login.html', title="Login", form=form)#, #providers=app.config['OPENID_PROVIDERS'])

#view all user
# @app.route('/viewprofile', methods=['GET', 'POST'])
# @login_required
# def viewprofile():
#   u = models.Profile.query.all()
#   return render_template("profiler2.html", title="View User GUI", u=u)

#feed
@app.route('/feed/<username>')
@login_required
def feed(username):
  user = models.Profile.query.filter_by(username=username).first()

  if user is None:
    flash('User %s not found.' % username)
    return redirect(url_for('index'))

  posts = [
    {'author': user, 'body': 'Test post #1'},
    {'author': user, 'body': 'Test post #2'}
  ]

  return render_template('feed.html', user=user, posts=posts, title='Feed')

#follow a user
@app.route('/follow/<username>')
@login_required
def follow(username):
  user = models.Profile.query.filter_by(username=username).first()
  
  if user is None:
    flash('User %s not found.' % username)
    return redirect(url_for('login'))
  
  if user == g.user:
    flash('You can\'t follow yourself!')
    return redirect(url_for('index'))

  u = g.user.follow(user)
  if u is None:
    flash('Cannot follow ' + username + '.')
    return redirect(url_for('index'))

  db.session.add(u)
  db.session.commit()

  flash('You are now follow ' + username + '!')
  return redirect(url_for('index'))


#unfollow a user
@app.route('/unfollow/<username>')
@login_required
def unfollow(username):
  user = models.Profile.query.filter_by(username=username).first()
  
  if user is None:
    flash('User %s not found!' % username)
    return redirect(url_for('index'))

  if user == g.user:
    flash('You can\'t follow yourself!')
    return redirect(url_for('index'))

  u = g.user.unfollow(user)

  if u is None:
    flash('Cannot unfollow ' + username + '.')
    return redirect(url_for('index'))

  db.session.add(u)
  db.session.commit()
  flash('You have stopped following ' + username + '.')
  return redirect(url_for('index'))


#get a GUI display of users
@app.route('/gui_profile/<idNo>', methods=['GET', 'POST'])
def gui_profile(idNo):
  a = models.Profile.query.get(idNo)
  return render_template('profiler3.html', userid= str(a.userid), fname=str(a.fname), lname=str(a.lname), username=str(a.username), sex=str(a.sex), age=str(a.age), highscore=str(a.highscore), tdollar=str(a.tdollar), profile_add_on=str(a.profile_add_on), files=[f for f in os.listdir('app/static') if f==str(a.image)][0])

#create new profile
@app.route('/signup', methods=['POST', 'GET'])
def signup():
  form = Profiler()
  if form.validate()==True and request.method == 'POST':
    filename = secure_filename(form.image.data.filename)
    form.image.data.save(os.path.join('app/static', filename))
    insert(form.fname.data, form.lname.data, form.username.data, form.email.data, form.password.data, form.sex.data, form.age.data, filename)
    
    return redirect(url_for('index'))
    
  return render_template('profiler1.html', title='Sign Up', form=form)


#list of profiles
@app.route('/profiles', methods=['POST', 'GET'])
@login_required
def profiles():
  u = models.Profile.query.all()
  lst_online=[]
  lst_offline=[]
  
  for i in u:
    if i.is_active() == True:
      dic = {'username':i.username}
      lst_online += [dic]

    else:
      dic = {'username':i.username}
      lst_offline += [dic]
    
  usr = {'users_online':lst_online, 'users_offline':lst_offline}
  j = jsonify(usr)
  return j

#view the current user profile
@app.route('/profile', methods=['GET', 'POST'])
@login_required
def profileVi():
    a = models.Profile.query.get(current_user.get_id())
    return render_template('profiler3.html', userid= str(a.userid), fname=str(a.fname), lname=str(a.lname), username=str(a.username), sex=str(a.sex), age=str(a.age), highscore=str(a.highscore), tdollar=str(a.tdollar), profile_add_on=str(a.profile_add_on), files=[f for f in os.listdir('app/static') if f==str(a.image)][0])


#update current user profile
@app.route('/profile/<update>', methods=['GET', 'POST'])
@login_required
def updater(update):
  if(update == 'update'):
    form = Profiler()
    if form.validate()==True and request.method == 'POST':
      filename = secure_filename(form.image.data.filename)
      form.image.data.save(os.path.join('app/static', filename))

      update(form.fname.data, form.lname.data, form.username.data, form.email.data, form.password.data, form.sex.data, form.age.data, filename)
  return render_template("profile_update.html", title="Profile Update", form=form)


#show list of games
@app.route('/games', methods=['GET'])
@login_required
def games():
  return render_template('games.html', title='List of Games', id=id)

#link to games
@app.route('/game/<int:id>', methods=['GET'])
@login_required
def game(id):
  if id == 1:
    return render_template('boilerplate.html', title="Boiler Plate")
  
  elif id == 2:
    return render_template('spaceinvader.html', title='Space Invader')

#confirmation code for user
@app.route('/signup/<confirm>', methods=['GET', 'POST'])
@app.route('/signup/<confirm>/<confirmcode>', methods=['GET'])
def confirmation(confirm, confirmcode):
  a = models.Profile.query.filter_by(confirmation_id=confirmcode).first()
  if a is None:
    #pdb.set_trace()
    return render_template('404.html', title='404')

  elif confirm=='confirm' and confirmcode==str(a.confirmation_id):
    models.Profile.query.filter_by(confirmation_id=confirmcode).update({'confirmation_status': 'Yes'})
    db.session.commit()
    return render_template('confirmation.html', title='Confirmation')
  

#check game highscores
@app.route('/game/<highscore>', methods=['GET', 'POST'])
@login_required
def highscore(highscore):
  return jsonify({ 
                  'highscore_1':unicode(current_user.highscore)#,
                  #'highscore_2':u.highscore2,
                 })

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge, chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=600'
    return response


@app.errorhandler(404)
def page_not_found(error):
    """Custom 404 page."""
    return render_template('404.html', title='404')


@app.route('/test', methods=['GET', 'POST'])
def test():
  if request.method == 'POST':
    req = request.json
    f = open("file.txt", "w+")
    f.write(req)
    f.close()
  return render_template('feed_test.html', title='Test Page')

@app.route('/blog/add/ajax', methods=['POST', 'GET'])
def add_blog_ajax():
  if request.method == 'POST':
    title = request.json['title']
    article = request.json['article']
    return jsonify(title=title, article=article)
  return render_template('form.html')

@app.route('/_add_numbers')
def add_numbers():
    """Add two numbers server side, ridiculous but well..."""
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    return jsonify(result=a + b)

@app.route('/layout2')
def layout2():
  return render_template('layout2.html')


if __name__ == '__main__':
  app.run(debug=True, host='0.0.0.0', port=8000)