from flask.ext.wtf import Form
from flask.ext.uploads import *
from wtforms import TextField, RadioField, DateField, FileField, PasswordField, SubmitField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Required, InputRequired
from wtforms import validators
from flask_wtf.file import FileField, FileAllowed, FileRequired
#from werkzeug import secure_filename
#from flask_wtf.file import FieldField 

class Profiler(Form):
  fname = TextField('fname', validators=[InputRequired()])
  lname = TextField('lname', validators=[InputRequired()])
  username = TextField('username', validators=[InputRequired()])
  email = EmailField('email', [validators.DataRequired(), validators.Email()])
  password = PasswordField('password', [validators.Required(), validators.EqualTo('confirm', message='Password mismatched')])
  confirm = PasswordField('confirm')
  sex = RadioField('Sex', choices=[('Male','Male'), ('Female', 'Female')], validators=[InputRequired()])
  age = DateField('age', format='%Y-%m-%d', validators=[DataRequired()])
  image = FileField('image', validators=[FileRequired(), FileAllowed(['jpg', 'png'], 'Images only!')])
  submit = SubmitField('submit', default=False)